#include <iostream>
#include <iomanip>
#include <cmath>
#include <locale.h>

using namespace std;

double integral (double a, double b, double n) {
	
	double resintegral, Xi, Deltax;

	Deltax = (b - a) / n;
	Xi = a - Deltax / 2;

	cout << "Deltax: " << Deltax << setprecision(6) << endl << endl; 
	
	for (int i = 0; i < n; i++)
	
	{
		Xi += Deltax;

		// resintegral += Deltax * (pow(Xi, 2)); // X^2
		// resintegral += Deltax * (pow(Xi, 3)); // X^3
		//resintegral += Deltax * 1/((2 * Xi)- 7); // 1/ 2 * x - 7
		// resintegral += Deltax * sqrt(pow(Xi, 3)+1); // Raiz de X ao cubo
		// resintegral += Deltax * (1/Xi); // 1 dividido por x
		// resintegral += Deltax * (pow(Xi, 2) + 3); // X ao quadrado mais 3
		// resintegral += Deltax * (1 / (2*Xi - 7)); // 1 dividido por 2x menos 7
		// resintegral += Deltax * sin(Xi);
		// resintegral += Deltax * cos(Xi);
		// resintegral += Deltax * sin(pow(Xi, 2));
		// resintegral += Deltax * cos(pow(Xi, 2));
		// resintegral += Deltax * tan(Xi);
		// resintegral += Deltax * (sqrt(pow(Xi, 4) + 2)); // raiz de x^4 mais 2s


		
		cout << "X" << i+1 << ": " << Xi << endl << endl;
		
	}
	
	cout << "Resultado ( Deltax * Σ( f(Xi) ) ): " << resintegral  << setprecision(6) << endl;
    
}

	
int main(){

    setlocale(LC_ALL,"portuguese");

	double a, b, n;

	cout << "Integral Numerica - Regra do Ponto Medio " << endl;
	cout << "Valor de a: "; cin >> a; //Entrada do usuario para a variavel "a", primeiro intervalo
	cout << "Valor de b: "; cin >> b; //Entrada do usuario para a variavel "b", segundo intervalo
	cout << "Número de retângulos: "; cin >> n; //Entrada do usuario para a variavel "N", número de retangulos
	cout << "\n";

	cout << "Os valores inseridos sao: " << endl;
	cout << "Intervalo a: " << a << endl; 
    cout << "Intervalo b: " << b << endl; 
	cout << "Número de Retãngulos: " << n << endl << endl; 

	integral(a, b, n);
	
	
return 0;
}
