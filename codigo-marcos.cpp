#include <iostream>
#include <iomanip>
#include <cmath>
#include <locale.h>

using namespace std;

double integral (double Deltax, double a, double b, double n, double Xi) {
	
	double funcao;
	
	Deltax = (b - a) / n;
	Xi = a - Deltax / 2;

	cout << "Deltax, xi" << endl;
	cout << Deltax << setprecision(4) << endl; 
    cout << Xi << endl << endl;
	
	for (int i = 0; i < n; i++)
	
	{
		Xi += Deltax;
		
		//funcao += Deltax * (pow(Xi, 3));
		//funcao += Deltax * 1/((2 * Xi)- 7);
		funcao += Deltax * sqrt(pow(Xi, 3)+1);
		
		cout << "Valores de Xi : " << Xi << endl << endl;
		
				cout << "DeltaX multiplicado pelo valor de Xi: " << funcao << setprecision(6) << endl << endl;

	}
	
	cout << "Resultado: " << funcao << endl;
    
}

	
int main(){

    setlocale(LC_ALL,"portuguese");

	double Deltax, a, b, 
    n, //Numero de retangulos
    Xi;

	cout << "Integral Num�rica - Regra do Ponto M�dio " << endl;
	cout << "Valor de a: "; cin >> a; //Entrada do usu�rio para a vari�vel "a"
	cout << "Valor de b: "; cin >> b; //Entrada do usu�rio para a vari�vel "b"
	cout << "Valor de N: "; cin >> n; //Entrada do usu�rio para a vari�vel "N"
	cout << "\n";

	cout << "Os valores inseridos s�o: " << endl;
	cout << "a: " << a << endl; 
    cout << "b: " << b << endl; 
	cout << "n: " << n << endl << endl; 

	integral(Deltax, a, b, n, Xi);
	
	
return 0;
}
